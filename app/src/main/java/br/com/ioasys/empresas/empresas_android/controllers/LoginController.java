package br.com.ioasys.empresas.empresas_android.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import br.com.ioasys.empresas.empresas_android.interface_services.LoginService;
import br.com.ioasys.empresas.empresas_android.models.Investor;
import br.com.ioasys.empresas.empresas_android.models.SharedAuthentication;
import br.com.ioasys.empresas.empresas_android.models.UserLogin;
import br.com.ioasys.empresas.empresas_android.retrofit.RetrofitSingleton;
import br.com.ioasys.empresas.empresas_android.util.Config;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by paulo on 03/08/17.
 */


public class LoginController {

    private String url_base;
    private JSONObject str = null;
    private static String saida;

    /*
        * LoginController responsavel pela autenticaçao
     */

    public LoginController(String url_base){
        this.url_base = url_base;
    }

    public Response<Investor> Authentication(UserLogin userLogin) {

        Response<Investor> resposta = null;

        final StringBuilder builder = new StringBuilder();
        builder.append("{email : ");
        builder.append(userLogin.getUser());
        builder.append(", password : ");
        builder.append(userLogin.getPassword());
        builder.append("}");

        String base = userLogin.getUser() + ":" + userLogin.getPassword();
        String header = "Basic " + Base64.encodeToString(base.getBytes(), Base64.NO_WRAP);

        Retrofit retrofit = RetrofitSingleton.getRetrofit(this.url_base);

        LoginService loginService = retrofit.create(LoginService.class);

        try {
            str = new JSONObject(builder.toString());
        } catch (JSONException e) {

        }
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/json") , str.toString());
        Call<Investor> call = loginService.loginInvestor(header , requestBody);

        try {
            resposta  = call.execute();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return resposta;
    }
    /*
        * saveToken Salva os dados do usuario
     */

    public boolean saveToken(SharedAuthentication sharedAuthentication){

        SharedPreferences.Editor editor = sharedAuthentication.getActivity()
                .getSharedPreferences(Config.PREFERENCES_NAME, Context.MODE_PRIVATE).edit();
        editor.putString("access-token" ,sharedAuthentication.getAccess_token());
        editor.putString("client", sharedAuthentication.getClient());
        editor.putString("uid" , sharedAuthentication.getUid());
        editor.commit();

        return true;
    }
}
