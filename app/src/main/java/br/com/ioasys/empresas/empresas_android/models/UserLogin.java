package br.com.ioasys.empresas.empresas_android.models;

/**
 * Created by paulo on 01/08/17.
 */

public class UserLogin {

    private String user;
    private String password;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
