package br.com.ioasys.empresas.empresas_android.models;

import android.app.Activity;

/**
 * Created by paulo on 05/08/17.
 */

public class SharedAuthentication {

    private String client;
    private String access_token;
    private String uid;
    private Activity activity;

    public SharedAuthentication(String client, String access_token, String uid, Activity activity) {
        this.client = client;
        this.access_token = access_token;
        this.uid = uid;
        this.activity = activity;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }
}
