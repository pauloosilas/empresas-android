package br.com.ioasys.empresas.empresas_android;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class EmpresaActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextView description;
    private String[] enterprise;
    private ImageView logo;
    private ImageView enterprise_photo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_empresa);
        logo = (ImageView) findViewById(R.id.logo_investor);
        logo.setVisibility(View.INVISIBLE);

        toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        setSupportActionBar(toolbar);
        description = (TextView) findViewById(R.id.enterprise_description);
        enterprise_photo = (ImageView) findViewById(R.id.enterprise_photo);

        Intent intent = getIntent();
        Bundle params = intent.getExtras();

        enterprise = params.getStringArray("enterprise");

        getSupportActionBar().setTitle(enterprise[7]);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        description.setText(enterprise[9]);
        if(enterprise[8] != null) {
            Glide.with(this)
                    .load(enterprise[8])
                    .into(enterprise_photo);
        }



        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkHome));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == android.R.id.home){
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
