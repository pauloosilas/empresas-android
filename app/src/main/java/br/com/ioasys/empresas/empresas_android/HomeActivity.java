package br.com.ioasys.empresas.empresas_android;


import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import br.com.ioasys.empresas.empresas_android.controllers.EnterpriseController;
import br.com.ioasys.empresas.empresas_android.controllers.LoginController;
import br.com.ioasys.empresas.empresas_android.fragments.EnterpriseInfo;
import br.com.ioasys.empresas.empresas_android.models.Enterprise;
import br.com.ioasys.empresas.empresas_android.models.SharedAuthentication;
import br.com.ioasys.empresas.empresas_android.retrofit.AsyncResponse;
import br.com.ioasys.empresas.empresas_android.util.Config;
import br.com.ioasys.empresas.empresas_android.util.MsgToast;

public class HomeActivity extends AppCompatActivity {


    private Toolbar toolbar;
    private EnterpriseController enterpriseController;
    private SharedAuthentication sharedAuthentication;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        MsgToast.toast(this, Config.AUTH_SUCESS);
        toolbar = (Toolbar) findViewById(R.id.toolbar_home);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDarkHome));
        }

        preferences = getSharedPreferences(Config.PREFERENCES_NAME , Context.MODE_PRIVATE);


        sharedAuthentication = new SharedAuthentication(preferences.getString("client" , null),
                                            preferences.getString("access-token", null),
                                            preferences.getString("uid", null),
                                            HomeActivity.this
              );

        enterpriseController = new EnterpriseController(Config.URL_ENTERPRISE_BASE , sharedAuthentication);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_search , menu);

        MenuItem item = menu.findItem(R.id.menuSearch);
        SearchView searchView = (SearchView) item.getActionView();


        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                new AsyncResponse(sharedAuthentication , Config.URL_ENTERPRISE_BASE).execute(newText);
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

}


