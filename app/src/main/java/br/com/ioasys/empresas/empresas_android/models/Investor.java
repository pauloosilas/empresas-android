package br.com.ioasys.empresas.empresas_android.models;

/**
 * Created by paulo on 03/08/17.
 */

public class Investor {

    private int id;
    private String investor_name;
    private String email;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInvestor_name() {
        return investor_name;
    }

    public void setInvestor_name(String investor_name) {
        this.investor_name = investor_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
