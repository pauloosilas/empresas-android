package br.com.ioasys.empresas.empresas_android.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import br.com.ioasys.empresas.empresas_android.EmpresaActivity;
import br.com.ioasys.empresas.empresas_android.HomeActivity;
import br.com.ioasys.empresas.empresas_android.R;
import br.com.ioasys.empresas.empresas_android.models.Enterprise;

/**
 * A simple {@link Fragment} subclass.
 */
public class EnterpriseInfo extends Fragment {

    private Enterprise enterprise;
    private TextView enterprise_name;
    private TextView enterprise_type;
    private TextView enterprise_country;
    private ImageView enterprise_photo;
    public EnterpriseInfo() {
        // Required empty public constructor
    }

    /*
        Este Fragmento e inflado com as informaçoes de uma empresa.
        Caso a busca por nome retorne um objeto Enterprise nao nulo,
        ele sera exibido ao usuario atravez deste.

     */

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.enterprise_container, container, false);

        final String[] infoArray =  this.getArguments().getStringArray("enterprise");


            enterprise_name = (TextView) view.findViewById(R.id.enterprise_name);
            enterprise_type = (TextView) view.findViewById(R.id.enterprise_type);
            enterprise_country = (TextView) view.findViewById(R.id.enterprise_country);
            enterprise_photo = (ImageView) view.findViewById(R.id.enterprise_photo);
        if(infoArray != null) {
            enterprise_name.setText(infoArray[7]);
            enterprise_type.setText(infoArray[15]);
            enterprise_country.setText(infoArray[11]);

            if(infoArray[8] != null) {
                Glide.with(this)
                        .load("http://www.iconesbr.net/iconesbr/2008/07/263/263_128x128.png")
                        .into(enterprise_photo);
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity().getApplicationContext() , EmpresaActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putStringArray("enterprise" , infoArray);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }

        return  view;
    }

    public void inflateEnterprise(Enterprise enterprise){

        enterprise_name.setText(enterprise.getEnterprise_name().toString());
        enterprise_name.setText(enterprise.getEnterprise_type().getEnterprise_type_name());
        enterprise_country.setText(enterprise.getCountry());

    }

}
