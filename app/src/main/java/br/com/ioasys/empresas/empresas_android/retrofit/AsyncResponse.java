package br.com.ioasys.empresas.empresas_android.retrofit;

import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import br.com.ioasys.empresas.empresas_android.R;
import br.com.ioasys.empresas.empresas_android.controllers.EnterpriseController;
import br.com.ioasys.empresas.empresas_android.fragments.EnterpriseInfo;
import br.com.ioasys.empresas.empresas_android.models.Enterprise;
import br.com.ioasys.empresas.empresas_android.models.SharedAuthentication;
import br.com.ioasys.empresas.empresas_android.util.Config;

/**
 * Created by paulo on 05/08/17.
 */
    /*
        Esta classe obtem a resposta assincrona do endpoint,
        para a busca da empresa pelo seu nome
     */
public class AsyncResponse extends AsyncTask<String, Enterprise, Enterprise> {

    private SharedAuthentication sharedAuthentication;
    private String url_base;
    private EnterpriseController enterpriseController;

    public AsyncResponse(SharedAuthentication sharedAuthentication, String url_base){
        this.sharedAuthentication = sharedAuthentication;
        this.url_base = url_base;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        enterpriseController = new EnterpriseController(url_base , sharedAuthentication);
    }

    @Override
    protected Enterprise doInBackground(String... strings) {
        Log.i("SAIDA" , strings[0]);
        Enterprise enterprise = enterpriseController.findEnterpriseByName(strings[0]);
        return enterprise;
    }

    @Override
    protected void onPostExecute(Enterprise enterprise) {
        super.onPostExecute(enterprise);

        if(enterprise != null) {

            EnterpriseInfo info = new EnterpriseInfo();

            android.app.FragmentTransaction transaction =
                    sharedAuthentication.getActivity().getFragmentManager().beginTransaction();

            transaction.replace(R.id.container, info);


            String[] infoArray = {
                    String.valueOf(enterprise.getId()),
                    enterprise.getEmail_enterprise(),
                    enterprise.getFacebook(),
                    enterprise.getTwitter(),
                    enterprise.getLinkedin(),
                    enterprise.getPhone(),
                    enterprise.getOwn_enterprise(),
                    enterprise.getEnterprise_name(),
                    enterprise.getPhoto(),
                    enterprise.getDescription(),
                    enterprise.getCity(),
                    enterprise.getCountry(),
                    String.valueOf(enterprise.getValue()),
                    String.valueOf(enterprise.getShare_price()),
                    String.valueOf(enterprise.getEnterprise_type().getId()),
                    enterprise.getEnterprise_type().getEnterprise_type_name()

            };
            Log.i("SAIDA", enterprise.getEnterprise_name());
            Bundle bundle = new Bundle();
            bundle.putStringArray("enterprise", infoArray);
            info.setArguments(bundle);
            info.setRetainInstance(true);
            transaction.setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            transaction.commit();
        }
    }
}