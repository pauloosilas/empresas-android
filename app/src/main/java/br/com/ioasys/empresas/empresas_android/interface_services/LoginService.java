package br.com.ioasys.empresas.empresas_android.interface_services;

import br.com.ioasys.empresas.empresas_android.models.Investor;
import br.com.ioasys.empresas.empresas_android.util.Config;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by paulo on 01/08/17.
 */

public interface LoginService {

    @POST(Config.SERVICE_NAME_LOGIN)
    public Call<Investor> loginInvestor(@Header("Autorization") String header, @Body RequestBody request);

}
