package br.com.ioasys.empresas.empresas_android.interface_services;

import br.com.ioasys.empresas.empresas_android.models.EnterpriseList;
import br.com.ioasys.empresas.empresas_android.models.Enterprise;
import br.com.ioasys.empresas.empresas_android.util.Config;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by paulo on 03/08/17.
 */

public interface EnterpriseService {


    @GET(Config.SERVICE_NAME_ENTERPRISE)
    public Call<Enterprise> enterprise(@Query("name") String name);

    @GET(Config.SERVICE_NAME_ENTERPRISE)
    public Call<EnterpriseList> enterpriseList(@Query("name") String name);


}
