package br.com.ioasys.empresas.empresas_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import br.com.ioasys.empresas.empresas_android.controllers.LoginController;
import br.com.ioasys.empresas.empresas_android.models.Investor;
import br.com.ioasys.empresas.empresas_android.models.SharedAuthentication;
import br.com.ioasys.empresas.empresas_android.models.UserLogin;
import br.com.ioasys.empresas.empresas_android.util.Config;
import br.com.ioasys.empresas.empresas_android.util.Connection;
import br.com.ioasys.empresas.empresas_android.util.MsgToast;
import br.com.ioasys.empresas.empresas_android.util.Validation;
import retrofit2.Response;

/*
        O APLICATIVO SE INICIA NESTA ACTIVITY

*/

public class LoginActivity extends AppCompatActivity {

    private EditText edtUser;
    private EditText edtPassword;
    private Button btnLogin;
    private UserLogin userLogin;
    private Response<Investor> response;
    private LoginController loginController;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        preferences = getSharedPreferences(Config.PREFERENCES_NAME, Context.MODE_PRIVATE);

        edtUser = (EditText) findViewById(R.id.editLogin);
        edtPassword = (EditText) findViewById(R.id.edtPassword);
        btnLogin = (Button) findViewById(R.id.buttonLogin);


        String login = preferences.getString("access-token", null);

        /* Autentica uma unica vez
        if(login != null){
            Intent it = new Intent(LoginActivity.this, HomeActivity.class);
            startActivity(it);
        }
        */

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                userLogin = new UserLogin();
                userLogin.setUser(edtUser.getText().toString());
                userLogin.setPassword(edtPassword.getText().toString());

                if(!Validation.isValidLogin(edtUser.getText().toString())) {
                    MsgToast.toast(LoginActivity.this, Config.TEXT_NULL);
                    return;
                }
                if(!Validation.isValidLogin(edtPassword.getText().toString())) {
                    MsgToast.toast(LoginActivity.this, Config.TEXT_NULL);
                    return;
                }

                if(Connection.isConnectivity(LoginActivity.this))
                  new AsyncResponse().execute();
                else
                    MsgToast.toast(LoginActivity.this, Config.ERROR_CONNECTION);
            }
        });


    }
    /*
        ESTA CLASSE AsyncResponse RECUPERA A RESPOSTA ASSINCRONA DE AUTENTICAÇAO
        COM O ENDPOINT

     */

    public class AsyncResponse extends AsyncTask<Response<Investor> ,Response<Investor> ,Response<Investor> >{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loginController = new LoginController(Config.URL_LOGIN_BASE);
        }

        @Override
        protected Response<Investor> doInBackground(Response<Investor>... responses) {

            Response<Investor> response = loginController.Authentication(userLogin);

            publishProgress(response);
            return response;
        }


        @Override
        protected void onPostExecute(Response<Investor> objectResponse) {
            super.onPostExecute(objectResponse);

            response = objectResponse;

            if(objectResponse != null)
                if(objectResponse.isSuccessful()){

                    SharedAuthentication shared =
                            new SharedAuthentication(response.headers().get("client"),
                                                     response.headers().get("access-token"),
                                                     response.headers().get("uid"),
                                                     LoginActivity.this);

                    loginController.saveToken(shared);
                    Log.i("SAIDA" , " " + objectResponse.raw());
                    Intent it = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(it);
                }
                else{
                    Log.i("SAIDA", " " + objectResponse.raw());
                    MsgToast.toast(LoginActivity.this, Config.LOGIN_ERROR);
                }

        }
    }
}
