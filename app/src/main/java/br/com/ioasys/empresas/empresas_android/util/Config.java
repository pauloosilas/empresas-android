package br.com.ioasys.empresas.empresas_android.util;

/**
 * Created by paulo on 03/08/17.
 */

public class Config {

    public static final String URL_LOGIN_BASE = "http://54.94.179.135:8090/api/v1/";
    public static final String SERVICE_NAME_LOGIN = "users/auth/sign_in";

    public static final String URL_ENTERPRISE_BASE = "http://54.94.179.135:8090/api/v1/";
    public static final String SERVICE_NAME_ENTERPRISE = "enterprises";

    public static final String PREFERENCES_NAME = "credentials";

    public static final String ERROR_CONNECTION = "Sem conexao com a internet";

    public static final String AUTH_SUCESS = "Bem Vindo! Voce ja esta logado!";

    public static final String TEXT_NULL = "Os campos devem ser preenchidos";

    public static final String LOGIN_ERROR = "E-mail ou Senha Incorreto(s)";


}
