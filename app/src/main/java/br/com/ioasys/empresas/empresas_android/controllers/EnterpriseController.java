package br.com.ioasys.empresas.empresas_android.controllers;

import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import java.io.IOException;

import br.com.ioasys.empresas.empresas_android.R;
import br.com.ioasys.empresas.empresas_android.fragments.EnterpriseInfo;
import br.com.ioasys.empresas.empresas_android.interface_services.EnterpriseService;

import br.com.ioasys.empresas.empresas_android.models.Enterprise;
import br.com.ioasys.empresas.empresas_android.models.EnterpriseList;
import br.com.ioasys.empresas.empresas_android.models.SharedAuthentication;
import br.com.ioasys.empresas.empresas_android.retrofit.RetrofitSingleton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by paulo on 03/08/17.
 */

public class EnterpriseController {

    private String url_base;
    private SharedAuthentication sharedAuthentication;

    public EnterpriseController(String url_base , SharedAuthentication sharedAuthentication){
        this.url_base = url_base;
        this.sharedAuthentication = sharedAuthentication;
    }
    /*
        * findEnterpriseByName busca uma empresa pelo nome
     */

    public Enterprise findEnterpriseByName(String name){

        Response<EnterpriseList> enterpriseList;
        Enterprise enterprise = null;


        RetrofitSingleton.retrofit = null;
        Retrofit retrofit  = RetrofitSingleton.getRetrofit(url_base, sharedAuthentication.getAccess_token(),
                                                                    sharedAuthentication.getClient(),
                                                                    sharedAuthentication.getUid());

        EnterpriseService enterpriseService = retrofit.create(EnterpriseService.class);
        final Call<EnterpriseList> enterpriseCall = enterpriseService.enterpriseList(name);

        try {
            enterpriseList = enterpriseCall.execute();
            if(enterpriseList.isSuccessful())
                if(enterpriseList.body().enterprises.size() > 0) {
                    enterprise = enterpriseList.body().enterprises.get(0);
                    Log.i("SAIDA" , enterprise.getEnterprise_name());
                }
        } catch (IOException e) {
           Log.i("SAIDA", "ERROR");
            enterprise = null;
        }

        return enterprise;
    }

}
