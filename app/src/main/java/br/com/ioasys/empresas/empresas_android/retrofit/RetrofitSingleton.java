package br.com.ioasys.empresas.empresas_android.retrofit;

import android.util.Log;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by paulo on 03/08/17.
 */

public class RetrofitSingleton {


    public static Retrofit retrofit ;

    public RetrofitSingleton(){ }

    /*
        Cria um objeto retrofit, caso nao exista
     */
    public static Retrofit getRetrofit(String url_base){

        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(url_base).addConverterFactory(GsonConverterFactory.create())
                        .build();
        }
        return  retrofit;
    }

    /*
        * Cria um objeto Retrofit para autenticaçao via token
     */

    public static Retrofit getRetrofit(String url_base, final String token ,final String client,final String uid){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();


        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Interceptor.Chain chain) throws IOException {
                Request original = chain.request();

                Request request = original.newBuilder()
                        .header("Content-Type", "application/json")
                        .header("access-token", token)
                        .header("client" , client)
                        .header("uid", uid)
                        .method(original.method(), original.body())
                        .build();

                return chain.proceed(request);
            }
        });

        OkHttpClient clientBuild = httpClient.build();


            retrofit = new Retrofit.Builder()
                    .baseUrl(url_base).addConverterFactory(GsonConverterFactory.create())
                    .client(clientBuild)
                    .build();

        return  retrofit;
    }
}
