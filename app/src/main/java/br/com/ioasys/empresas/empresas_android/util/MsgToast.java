package br.com.ioasys.empresas.empresas_android.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by paulo on 07/08/17.
 */

public class MsgToast {

    public static void toast(Context context, String mensagem){
        Toast.makeText(context, mensagem, Toast.LENGTH_LONG).show();
    }
}
