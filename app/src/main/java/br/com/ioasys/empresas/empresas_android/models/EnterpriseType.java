package br.com.ioasys.empresas.empresas_android.models;

/**
 * Created by paulo on 05/08/17.
 */

public class EnterpriseType {

    private int id;
    private String enterprise_type_name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEnterprise_type_name() {
        return enterprise_type_name;
    }

    public void setEnterprise_type_name(String enterprise_type_name) {
        this.enterprise_type_name = enterprise_type_name;
    }
}
